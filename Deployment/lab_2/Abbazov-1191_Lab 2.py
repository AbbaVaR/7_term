import os
import math
import cmath
import pytest


def bool_to_str(bool_val: bool) -> str:
    """
    Конвертирует булевое значение в строку.
    :param bool_val: bool
    :return: str

    >>> bool_to_str(True)
    'True'
    >>> bool_to_str(False)
    'False'
    """
    return str(bool_val)


def test_bool_to_str():
    assert bool_to_str(True) == 'True'
    assert bool_to_str(False) == 'False'


def reverse_str(text: str) -> str:
    """
    Возвращает строку в обратном порядке
    :param text: str
    :return: str

    >>> reverse_str('test')
    'tset'
    >>> reverse_str('Hello, world!')
    '!dlrow ,olleH'
    """
    return text[::-1]


def test_reverse_str():
    assert reverse_str('test') == 'tset'
    assert reverse_str('Hello, world!') == '!dlrow ,olleH'


def sum_positive_num(num_list: list) -> int:
    """
    Возвращает сумму всех положительных значений в списке
    :param num_list: list
    :return: int

    >>> sum_positive_num([])
    0
    >>> sum_positive_num([-1,-5,-7])
    0
    >>> sum_positive_num([1,2,3,-5])
    6
    """
    sum_val = sum([num for num in num_list if num >= 0])
    return sum_val


def test_sum_positive_num():
    assert sum_positive_num([-1,-5,-7]) == 0
    assert sum_positive_num([1,2,3,-5]) == 6


def draw_triangle(row_num: int) -> str:
    """
    Выводит треугольник из символов *
    :param row_num: int
    :return: str

    >>> draw_triangle(1)
    '*\\n'
    >>> draw_triangle(3)
    '  *\\n ***\\n*****\\n'
    >>> draw_triangle(4)
    '   *\\n  ***\\n *****\\n*******\\n'
    """
    row_num = round(row_num)
    result = ''
    for idx in range(1,row_num+1):
        spaces = ' '*(row_num-idx)
        star_num = idx+(idx-1)
        result += spaces+'*'*star_num+'\n'
    return result


def test_draw_triangle():
    assert draw_triangle(1) == '*\n'
    assert draw_triangle(4) == '   *\n  ***\n *****\n*******\n'


def count_dublicates(text:str) -> int:
    """
    Возвращает количество символов, которые встречаются в строке более одного раза
    :param text: str
    :return: int

    >>> count_dublicates('fffy')
    1
    >>> count_dublicates('sadasffddfgfhjhgu')
    6
    """
    text = text.lower()
    dubl_count = 0
    for char in set(text):
        char_count = text.count(char)
        if char_count > 1:
            dubl_count += 1
    return dubl_count


def test_count_dublicates():
    assert count_dublicates('fffy') == 1
    assert count_dublicates('sadasffddfgfhjhgu') == 6


def camel_to_kebab(text: str) -> str:
    """
    Преобразует данную строку в стиль kebab-case
    :param text: str
    :return: str

    >>> camel_to_kebab('camelsHaveThreeHumps')
    'camels-have-three-humps'
    >>> camel_to_kebab('testCamelCase')
    'test-camel-case'
    """
    upper_chars = [char for char in text if char.isupper()]
    for char in upper_chars:
        text = text.replace(char, f'-{char.lower()}')
    return text


def test_camel_to_kebab():
    assert camel_to_kebab('camelsHaveThreeHumps') == 'camels-have-three-humps'
    assert camel_to_kebab('testCamelCase') == 'test-camel-case'


def count_nums(num: int) -> int:
    """
    Преобразует число в другое
    :param num: int
    :return: int

    >>> count_nums(0)
    10
    >>> count_nums(225554)
    223514
    """
    if type(num) != int:
        raise Exception('Input must be int')
    str_num = str(num)
    used_num = []
    result = ''
    for char in str_num:
        if char in used_num:
            continue
        char_count = str_num.count(char)
        used_num.append(char)
        result += f'{char_count}{char}'
    return int(result)


def test_count_nums():
    assert count_nums(0) == 10
    assert count_nums(225554) == 223514


def sort_odd(num_list: list) -> list:
    """
    Cортирует все нечетные числа в массиве по возрастанию
    :param num_list: list
    :return: list

    >>> sort_odd([7, 8, 1])
    [1, 8, 7]
    >>> sort_odd([5, 8, 6, 3, 4])
    [3, 8, 6, 5, 4]
    """
    even_dict = {}
    for idx, num in enumerate(num_list):
        if num%2 == 0:
            even_dict[idx]=num
    odd_list = [num for num in num_list if num%2==1]
    odd_list.sort()
    for key in even_dict.keys():
        odd_list.insert(key, even_dict[key])
    return odd_list


def test_sort_odd():
    assert sort_odd([7, 8, 1]) == [1, 8, 7]
    assert sort_odd([5, 8, 6, 3, 4]) == [3, 8, 6, 5, 4]


def write_to_file(text, out_path: str) -> str:
    text = str(text)
    dir_path = os.path.dirname(out_path)
    os.makedirs(dir_path, exist_ok=True)
    with open(out_path, 'a') as file:
        file.write(text + '\n')


def test_write_to_file(tmpdir):
    file = tmpdir.join('output.txt')
    write_to_file(text='Hello', out_path=file.strpath)
    assert file.read() == 'Hello\n'

    file = tmpdir.join('output_2.txt')
    write_to_file(text='Test test', out_path=file.strpath)
    assert file.read() == 'Test test\n'


def solve(a,b,c, complex):
    """
    >>> solve(1, 4, 3, False)
    'Корни уравнения: -1.0 -3.0'
    >>> solve(1, 4, 3, True)
    'Комплексные корни уравнения: (-1+0j) (-3+0j)'
    >>> solve(0, 4, 3, True)
    "Argument 'a' must be != 0"
    """
    if a == 0:
        return "Argument 'a' must be != 0"

    d = b ** 2 - 4 * a * c
    if complex:
        x1 = (-b + cmath.sqrt(d)) / (2 * a)
        x2 = (-b - cmath.sqrt(d)) / (2 * a)
        return f"Комплексные корни уравнения: {x1} {x2}"
    else:
        if d < 0:
            return "Нет решения"
        elif d == 0:
            x = -b / 2 * a
            return f"Корень уравнения: {x}"
        else:
            x1 = (-b + math.sqrt(d)) / (2 * a)
            x2 = (-b - math.sqrt(d)) / (2 * a)
            return f"Корни уравнения: {x1} {x2}"


def test_solve():
    assert solve(1, 4, 3, False) == 'Корни уравнения: -1.0 -3.0'
    assert solve(1, 4, 3, True) == 'Комплексные корни уравнения: (-1+0j) (-3+0j)'
