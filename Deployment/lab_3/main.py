import math
import cmath
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from typing import Union, List

app = FastAPI()

class Item(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None


class Task3List(BaseModel):
    num_list: List[Union[int, float]]


class Task7List(BaseModel):
    num_list: List[int]


class Task11(BaseModel):
    a : Union[int, float]
    b : Union[int, float]
    c : Union[int, float]
    complex_flag : bool

@app.get("/")
def root():
    return {"message": "Hello, World!"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    if q:
        return {"item_id": item_id, "q": q}
    return {"item_id": item_id}


@app.post("/items/")
async def create_item(item: Item):
    item_dict = item.dict()
    if item.tax:
        price_with_tax = item.price + item.tax
        item_dict.update({"price_with_tax": price_with_tax})
    return item_dict


@app.get("/task1/{bool_val}")
def bool_to_str(bool_val: bool) -> str:
    return str(bool_val)


@app.get("/task2/{text}")
def reverse_str(text: str) -> str:
    return text[::-1]


@app.post("/task3/")
def sum_positive_num(num_list: Task3List) -> int:
    sum_val = sum([num for num in num_list.num_list if num >= 0])
    return sum_val


@app.get("/task4/{row_num}")
def draw_triangle(row_num: int) -> str:
    row_num = round(row_num)
    result = ''
    for idx in range(1,row_num+1):
        spaces = ' '*(row_num-idx)
        star_num = idx+(idx-1)
        result += spaces+'*'*star_num+'\n'
    return result


@app.get("/task5/{text}")
def count_dublicates(text:str) -> int:
    text = text.lower()
    dubl_count = 0
    for char in set(text):
        char_count = text.count(char)
        if char_count > 1:
            dubl_count += 1
    return dubl_count


@app.get("/task6/{text}")
def camel_to_kebab(text: str) -> str:
    upper_chars = [char for char in text if char.isupper()]
    for char in upper_chars:
        text = text.replace(char, f'-{char.lower()}')
    return text


@app.get("/task7/{num}")
def count_nums(num: int) -> int:
    if type(num) != int:
        raise Exception('Input must be int')
    str_num = str(num)
    used_num = []
    result = ''
    for char in str_num:
        if char in used_num:
            continue
        char_count = str_num.count(char)
        used_num.append(char)
        result += f'{char_count}{char}'
    return int(result)


@app.post("/task8/")
def sort_odd(num_list: Task7List) -> list:
    even_dict = {}
    for idx, num in enumerate(num_list.num_list):
        if num%2 == 0:
            even_dict[idx]=num
    odd_list = [num for num in num_list.num_list if num%2==1]
    odd_list.sort()
    for key in even_dict.keys():
        odd_list.insert(key, even_dict[key])
    return odd_list


@app.post("/task11/")
def solve(values: Task11):
    a = values.a
    b = values.b
    c = values.c
    complex_flag = values.complex_flag

    d = b ** 2 - 4 * a * c
    if a == 0:
        return None
    if complex_flag:
        x1 = (-b + cmath.sqrt(d)) / (2 * a)
        x2 = (-b - cmath.sqrt(d)) / (2 * a)
        return str(x1), str(x2)
    else:
        if d < 0:
            return None
        elif d == 0:
            x = -b / 2 * a
            return x
        else:
            x1 = (-b + math.sqrt(d)) / (2 * a)
            x2 = (-b - math.sqrt(d)) / (2 * a)
            return x1, x2


if __name__ == "__main__":
    uvicorn.run("main:app", port=5000, log_level="info", reload=True, debug=True)